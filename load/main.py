# coding=utf-8

import conf
import sys, os
sys.path.append(os.path.join(os.path.split(os.path.realpath(__file__))[0], '..'))
from common import filetool


if __name__ == '__main__':
    gen_dir = 'load_data'
    sql_queue = []
    
    # create 语句生成
    filetool.reopen(gen_dir, 'create.sql')
    sql_queue.append('create.sql') 

    for table in conf.tables:
        print 'use %s;' % conf.database

        # create 语句生成
        print 'create table if not exists %s (' % table[0].strip()
        for i in range(1, len(table)-1):
            if i > 1:
                print ','
            print '\t' + table[i].strip(),
        print '\n);'
        for index in [x.strip() for x in table[-1].split(',')]:
            print 'alter table %s add index(%s);' % (table[0].strip(), index)
        print ''
    
    # load语句生成
    for item in conf.files:
        script = 'load_%s.sql' % item[1]
        sql_queue.append(script)
        filetool.reopen(gen_dir, script)
        print 'use ijcai;'
        print 'load data local infile \'%s\'' % item[0]
        print 'into table %s' % item[1]
        print 'character set utf8'
        print 'fields terminated by \'%s\'' % conf.fields_terminater
        if conf.enclose:
            print 'enclosed by \'%s\'' % conf.enclose
        print 'lines terminated by \'%s\'' % conf.lines_terminater
        print ';'

    filetool.reopen(gen_dir, 'run.sh')
    for script in sql_queue:
        print 'mysql --local-infile -u%s -p%s < %s' % (conf.user, conf.password, script)
