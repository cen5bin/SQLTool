# coding=utf-8


user = 'liqiang'
password = '123456'

database = 'ijcai'
fields_terminater = ','
enclose = None # 如果文件中的字段用""包起来了就设置成"
lines_terminater = '\\n'


# format
# (tablename, colname1, colname2, ..., colnameN, index cols)
tables = [
    ('behavior', 'uid int not null', 'sid int not null', 'iid int not null', 'cid int not null', 'aid int not null', 'timestamp date', 'uid,sid,iid,cid,aid,timestamp'),
    ('records', 'uid int not null', 'mid int not null', 'lid int not null',  'timestamp date', 'uid,mid, lid,timestamp'),
    ('smerchant', 'mid int not null', 'bugget int not null', 'lid int not null', 'uid,bugget, lid'),
    ('test', 'uid int not null', 'lid int not null', 'uid, lid'),
]


files = [
    ('/home/liqiang/ijcai/data/ijcai2016_koubei_test', 'test'),
    ('/home/liqiang/ijcai/data/ijcai2016_koubei_train', 'records'),
    ('/home/liqiang/ijcai/ttt', 'smerchant'),
    ('/home/liqiang/ijcai/data/ijcai2016_taobao', 'behavior'),

]
