#!/usr/bin/python
# coding=utf-8

import sys, os
from common import sqlutil, filetool


#func = {
#    'null2zero' : sqlutil.null2zero,
#    'null2inf' : sqlutil.null2inf,
#}



def showHelp():
    ll = 30
    print '%s %s %s' % ('-' * ll, 'SQLTool 使用帮助', '-' * ll)
    print '-h, --help: 查看帮助'
    print 'init: 初始化, 初始化之前请先配置configure.py'
    print 'getcols <database> <table>: 获取表database.table的所有列'
    print '-s <sql script>: 运行一个sql脚本' 
    print '-d <sql script dir>: 运行目录下的所有sql脚本'
    print 'bigjoin <join_conf>: 运行bigjoin，需要指定join的配置文件'

    print '-' * 78

def aaa():
    print 'zzz'


if __name__ == '__main__':
    if len(sys.argv) == 1:
        showHelp()
    elif sys.argv[1] == '-h' or sys.argv[1] == '--help':
        showHelp()
    elif sys.argv[1] == 'init':
        os.system('python %s' % filetool.initMain)
    elif sys.argv[1] == 'getcols':
        if len(sys.argv) < 4:
            showHelp()
        else:
            os.system('sh %s %s %s' % (filetool.getColsShell, sys.argv[2], sys.argv[3]))
    elif sys.argv[1] == '-s':
        if len(sys.argv) < 3:
            showHelp()
        else:
            os.system('sh %s %s' % (filetool.runSQLShell, sys.argv[2]))
    elif sys.argv[1] == '-d':
        if len(sys.argv) < 3:
            showHelp()
        else:
            os.system('sh %s %s' % (filetool.runDirShell, sys.argv[2]))
    elif sys.argv[1] == 'bigjoin':
        if len(sys.argv) < 3:
            showHelp()
        else:
            os.system('python %s %s' % (filetool.bigjoinMain, sys.argv[2]))
