username={{username}}
password={{password}}

###################### Don't modify anython below ########################

if [ $# != 2 ]; then
	echo "请指定[database] [table]"
	exit
fi

database=$1
table=$2

echo "use $database; desc $table;" | mysql --local-infile -u$username -p$password | sed -n '2,$p' | awk '{print $1}' | tr '\n' ',' | tr '\t' ' '
echo ""
