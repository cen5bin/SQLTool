username={{username}}
password={{password}}
sysuser={{sysuser}}
sysgroup={{sysgroup}}

###################### Don't modify anython below ########################

if [ `whoami` != "root" ]; then
	echo 此脚本必须用root用户运行
	exit
fi

if [ $# = 0 ]; then
    echo 请指定数据库名和表名
    exit
fi

database=$1
table=$2

outfile="$database"_"$table".dat

echo `echo "use $database; desc $table;" | mysql -u$username -p$password | sed -n '2,$p' | awk '{print $1}'` | tr ' ' ',' > $outfile

tmp_file=/tmp/$outfile
rm -f $tmp_file

echo "use $database; select * into outfile '$tmp_file' fields terminated by ',' from $table ; " | mysql -u$username -p$password 

cat $tmp_file >> $outfile
chown $sysuser:$sysgroup $outfile
rm -f $tmp_file

