# coding=utf-8
import os, sys
sys.path.append(os.path.join(os.path.split(os.path.realpath(__file__))[0], '..'))
from common import filetool, configure


def sed(placehold, val):
    return 'sed \'s/{{%s}}/%s/g\'' % (placehold, val)


if __name__ == '__main__':
    filetool.reopen('init', 'init.sql')
    for db in configure.databases:
       print 'create database if not exists %s' % (db)
    for u in configure.users:
       print 'create user \'%s\'@\'localhost\' identified by \'%s\';' % (u[0], u[1])
       for db in configure.databases:
               print 'grant all privileges on %s.* to \'%s\'@\'%s\';' % (db, u[0], u[1])


    username = configure.users[0][0]
    password = configure.users[0][1]

    sed_username = sed('username', username)
    sed_password = sed('password', password)
    sed_sysuser = sed('sysuser', configure.sys_user)
    sed_sysgroup = sed('sysgroup', configure.sys_group)

    sed_root = sed('username', 'root')
    sed_root_password = sed('password', configure.db_root_password)

    os.system('cat %s | %s | %s | %s | %s > %s' % (filetool.dumpTemp, sed_username, sed_password, sed_sysuser, sed_sysgroup, filetool.dumpShell))
    os.system('cat %s | %s | %s > %s ' % (filetool.runSQLTemp, sed_username, sed_password, filetool.runSQLShell))
    os.system('cat %s | %s | %s > %s ' % (filetool.runSQLTemp, sed_root, sed_root_password, filetool.runSQLAsRootShell))
    os.system('cat %s | %s | %s > %s ' % (filetool.getColsTemp, sed_username, sed_password, filetool.getColsShell))

    #filetool.stdout.write('init执行完成，相关脚本存在目录 %s 下' % filetool.realdir('init'))
    filetool.println('init执行完成，相关脚本存在目录: %s' % filetool.realdir('init'))

    if len(sys.argv) == 2 and sys.argv[1] == '-r':
        filetool.runSql(filetool.realpath('init', 'init.sql'))
