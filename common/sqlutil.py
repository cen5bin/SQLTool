# coding=utf-8

import os

# 获取数据库表的所有字段
def get_all_cols(user, password, table):
    tmp_file = '/tmp/%s.col' % table
    script = 'echo "use ijcai; desc %s;" | mysql -u%s -p%s | sed -n \'2,$p\' | awk \'{print $1}\' > %s' % (table, user, password, tmp_file)
    os.system(script)
    f = open(tmp_file, 'r') 
    lines = f.readlines()
    f.close()
    if os.path.exists(tmp_file):
        os.remove(tmp_file)
    return [x.rstrip('\r\n') for x in lines]

def drop_table(table):
    print 'drop table if exists %s;' % table

def create_table(table):
    print 'create table %s as' % table

# 产生select语句，table为表名，sql为select语句，drop表示是否在创建前删除
def gen_select(table, sql, drop=False):
    if drop:
        drop_table(table)
    create_table(table)
    print sql
    if sql[-1] != ';':
        print ';'

def gen_add_index(table, keys):
    for key in keys:
        print 'alter table %s add index(%s);' % (table, key)

# 创建索引，keys表示要加索引的列名数组
def add_index(table, keys):
    for key in keys:
        print 'alter table %s add index(%s);' % (table, keys)

def null2val(colname, val):
    print 'if(%s is null, %s, %s)' % (colname, str(val), colname)

def null2zero(colname):
    null2val(colname, 0)

