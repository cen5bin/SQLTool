# coding=utf-8

import os, sys

def parentDir(path):
    return os.path.realpath(os.path.join(os.path.realpath(os.path.split(path)[0]), '..'))

def projectRoot():
    return parentDir(__file__)

stdout = sys.__stdout__

projectRoot = parentDir(__file__)

initRoot = os.path.join(projectRoot, 'init')
initMain = os.path.join(initRoot, 'main.py')

bigjoinRoot = os.path.join(projectRoot, 'bigjoin')
bigjoinMain = os.path.join(bigjoinRoot, 'main.py')

templateRoot = os.path.join(projectRoot, 'template')
dumpTemp = os.path.join(templateRoot, 'dump_table.sh')
runSQLTemp = os.path.join(templateRoot, 'runsql.sh')
getColsTemp = os.path.join(templateRoot, 'get_all_cols.sh')

shellRoot = os.path.join(projectRoot, 'shell')
dumpShell = os.path.join(shellRoot, 'dump_table.sh')
runSQLShell = os.path.join(shellRoot, 'runsql.sh')
runDirShell = os.path.join(shellRoot, 'rundir.sh')
runSQLAsRootShell = os.path.join(shellRoot, 'runsql_as_root.sh')
getColsShell = os.path.join(shellRoot, 'get_all_cols.sh')

genRoot = os.path.join(projectRoot, 'gen_script')

logRoot = os.path.join(projectRoot, 'log')
if not os.path.isdir(logRoot):
	os.makedirs(logRoot)

if not os.path.isdir(genRoot):
    os.makedirs(genRoot)

def mkdir(dirname):
    path = os.path.join(genRoot, dirname)
    if not os.path.isdir(path):
        os.makedirs(path)

def realpath(dirname, filename):
    return os.path.realpath(os.path.join(os.path.join(genRoot, dirname), filename))


def reopen(dirname, filename, mode='w'):
    mkdir(dirname)
    sys.stdout = open(realpath(dirname, filename), mode)
     
def realdir(dirname):
    return os.path.realpath(os.path.join(genRoot, dirname))

def cat(dirname, filename):
    os.system('cat %s' % realpath(dirname, filename))

def randomcat(dirname):
    files = os.listdir(realdir(dirname))
    files.sort()
    cat(dirname, files[0])

def println(text):
    stdout.write(text + '\n')

def runSql(filename, async=True):
    shell = 'sh %s %s' % (runSQLShell, filename)
    log = os.path.join(logRoot, os.path.split(filename)[1]) + '.log'
    if async:
    	shell = 'nohup %s > %s 2>&1 &' % (shell, log)
    os.system(shell)
    if async:
        println('开始运行%s' % filename)
        println('日志在%s' % log)

def runSqlAsRoot(filename, async=True):
    shell = 'sh %s %s' % (runSQLAsRootShell, filename)
    if async:
    	shell = 'nohup %s > %s.log 2>&1 &' % (shell, os.path.join(logRoot, filename))
    os.system(shell)

def runDir(dirname):
    logdir = os.path.join(logRoot, dirname)
    if not os.path.isdir(logdir):
        os.makedirs(logdir)
    log = os.path.join(logdir, 'run.log')
    shell = 'nohup sh %s %s > %s 2>&1 &' % (runDirShell, realdir(dirname), log)
    #println(shell)
    os.system(shell)
