# coding=utf-8
import os, imp
def load_module(path):
    dirname, filename = os.path.split(path)
    filename, ext = os.path.splitext(filename)
    module = imp.load_source(filename, path)
    return module
