# SQLTool
方便机器学习类竞赛开发的工具，利用python生成SQL代码提取特征，尽量把繁琐的特征工程自动化

## 模块介绍
- init: 初始化，设置数据库，新建用户等
- load: 加载数据
- bigjoin: 最终将特征join到一起
- recorder: 记录操作，支持replay
- common: 一些通用代码
- template: 一些模板文件，用于生成具体的一些文件
- shell: 一些shell脚本，包括数据导出，sql执行等

## 使用说明
- configure.py为全局配置
