if [ $# = 0 ]; then
	echo 请指定目录
	exit
fi

base=`dirname $0`
log_dir=$base/../log/`basename $1`
mkdir -p $log_dir

for sql in `ls $1/*.sql`; do
{
	sh $base/runsql.sh $sql > $log_dir/`basename ${sql}`.log 2>&1 
	echo finish $sql
}&
done
wait
echo finish all
