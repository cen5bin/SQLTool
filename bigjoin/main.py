# coding=utf-8

import os, sys
sys.path.append(os.path.join(os.path.split(os.path.realpath(__file__))[0], '..'))

from common import loadmodule, sqlutil, filetool



if __name__ == '__main__':
    if len(sys.argv) == 1:
        print '请指定配置文件'
        exit()

    conf = loadmodule.load_module(sys.argv[1])
    filetool.reopen('bigjoin', conf.output + '.sql') 
    print 'use %s;' % (conf.database)
    print 'drop table if exists %s;' % conf.output
    print 'create table %s as' % conf.output
    print 'select'
    for i in range(0, len(conf.tables)):
        table = conf.tables[i]
        cols = sqlutil.get_all_cols(conf.user, conf.password, table[0])
        ignore = []
        if len(table) > 4:
            ignore = [x.strip() for x in table[4].split(',')]
        cols = [x for x in cols if x not in ignore]
        for j in range(0, len(cols)):
            col = cols[j]
            print '%s.%s as %s_%s' % (table[1], col, table[0], col),
            if i != len(conf.tables) - 1 or j != len(cols) - 1:
                print ',',
        print '\n'

    print 'from %s %s' % (conf.tables[0][0], conf.tables[0][1])
    for i in range(1, len(conf.tables)):
        table = conf.tables[i]
        if table[2] == 'left' or table[2] == 'right':
            print '%s outer join' % table[2],
        else:
            print 'inner join'
        print '%s %s on' % (table[0], table[1]),
        if len(table) > 3:
            print table[3]
        else:
            print '1=1'

    print ';'
    if len(sys.argv) == 3 and sys.argv[2] == '-r':
        filetool.runSql(filetool.realpath('bigjoin', conf.output + '.sql'))
