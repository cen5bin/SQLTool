# coding=utf-8

# 数据库配置
database = 'ijcai'
user='liqiang'
password='123456'


output = 'test_big_join' # join完之后的表名

# 每个元组的格式
#('表名', '别名', '连接方式', '连接条件', '需要去掉的字段')
tables=[
    ('uml_11', 't1'),
    ('fea_loc_7_10', 't2', 'left', 't1.lid=t2.lid', 'lid'),
]

null_convert = {
    't2' : {
        '100000' : ('contains', '100'),
        '10000' : ('=', 'r_u_cnt_2,u_mid_cnt_2'),
        '0' : ('*'),
    }

}
